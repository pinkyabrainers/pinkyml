from functools import lru_cache


@lru_cache(maxsize=None)
def double_factorial(n):
    return n * double_factorial(n - 2) if n > 1 else 1
