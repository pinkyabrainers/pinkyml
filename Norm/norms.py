import numpy as np


def _lp_norm_vector(vector, p=2):
    """ compute lp norm of vector in R^n
    :param vector: numpy.array with shape (n,)
    :param p: float >= 1, for p = -1 is like p = infinity
    :return: float scalar, lp norm of vector
    """
    if p == -1:
        return np.abs(vector).max()
    elif p == 1:
        return np.abs(vector).sum()
    else:
        return np.power(np.power(np.abs(vector), p).sum(), 1.0/p)


def _lp_norm_function(function, area, weight, p=2):
    """ compute lp norm of function
    :param function: callable function to integrate
    :param area: callable is a function defining the area of integration
    :param p: float >= 1, for p = -1 is like p = infinity
    :return: float scalar, lp norm of function
    """
    # TODO write a function determining the mesh from area and integrate this!!!
    pass


def lp_norm(vector, area=None, weight=None, p=2):
    """ compute lp norm of vector in R^n or function. for vectors in R^n to be used only with vector and p arguments
    :param vector: if callable function to integrate, else numpy.array with shape (n,)
    :param area: callable is a function defining the area of integration
    :param weight: callable weight function
    :param p: float >= 1, for p = -1 equal p = infinity
    :return: float scalar, lp norm of function
    """
    if callable(vector):
        return _lp_norm_function(vector, area, weight, p)
    else:
        return _lp_norm_vector(vector, p)
