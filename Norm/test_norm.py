from unittest import TestCase
import numpy as np
import torch

from norms import lp_norm


class TestNorm(TestCase):
    """
    l norms should works on numpy.array and pytorch.tensor
    """
    def test_lp_norm_tensor_computing_able(self):
        vector = np.array([1, 1, 1, 1, 1])
        tensor = torch.tensor([1, 1, 1, 1, 1])
        self.assertAlmostEqual(lp_norm(vector, p=2), np.sqrt(5))
        self.assertAlmostEqual(lp_norm(tensor), np.sqrt(5))

    def test_lp_norm_different_p(self):
        vector1 = np.array([0, 1, 2, 3, 4])
        vector2 = np.array([0, 4, -2, 3, -5])
        p_array = [-1, 1, 2, 3]
        correct = [[4, 5], [10, 14], [np.sqrt(30), np.sqrt(54)], [np.power(100, 1.0/3), np.power(224, 1.0/3)]]
        for p, correct_results in zip(p_array, correct):
            self.assertAlmostEqual(lp_norm(vector1, p=p), correct_results[0])
            self.assertAlmostEqual(lp_norm(vector2, p=p), correct_results[1])
