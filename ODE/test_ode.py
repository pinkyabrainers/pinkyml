from unittest import TestCase
import numpy as np
import ode


class Test(TestCase):
    def test_euler_scheme(self):
        args = {"one": [lambda x: x[1], 1, 0, 100, 1],
                "two": [lambda x: x[1], 1, 0, 5, 1],
                "tree": [lambda x: 1, 0, 0, 12, 1],
                "four": [lambda x: 1, 0, 0, 12, 2]}
        results = {"one": [np.power(1.01, k) for k in range(101)],
                   "two": [np.power(1.01, k) for k in range(6)],
                   "tree": [k / 12 for k in range(13)],
                   "four": [k / 12 for k in range(25)]}
        for key in args.keys():
            arg = args[key]
            self.assertEquals(ode.euler_scheme(arg[0], arg[1], arg[2], arg[3], arg[4]), results[key])
