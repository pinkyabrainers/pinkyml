import numpy as np
from Norm.norms import lp_norm


class BrownianMotion:
    """
    A Brownian motion class
    """

    def __init__(self, dim=1, steps=5000, save_trace=False):
        """
        :param dim: int, space dimension
        :param steps: int, number of steps in the time interval [0,1]
        :param save_trace: bool, if True, saves all movement instead of the last position
        """
        self.dim = dim
        self.save_trace = save_trace
        self.trace = []
        self.point = np.zeros(shape=(self.dim,))
        self.steps = steps

    def clean_trace(self):
        """ clears the saved track
        :return:
        """
        self.trace = []

    def random_walk_step(self):
        """ moves one step
        :return:
        """
        if self.save_trace:
            self.trace.append(np.copy(self.point))
        self.point += np.array([element + np.random.choice([1, -1]) for element in self.point]) / np.sqrt(self.steps)

    def random_walk_normal_step(self):
        """ moves one step with normal distributions
        :return:
        """
        if self.save_trace:
            self.trace.append(np.copy(self.point))
        self.point += np.random.normal(loc=0.0, scale=1 / np.sqrt(self.steps), size=self.point.shape)

    @staticmethod
    def is_in_ball(point, center, radius, norm=lp_norm):
        """ checks if the point belongs to an open sphere with a given center and radius
        :param point: np.array with shape (n,), checked point
        :param center: np.array with shape (n,), center of the ball
        :param radius: float, sphere radius
        :param norm: callable, any function that is a norm
        :return: bool, True, if the point is in the ball
        """
        if norm(point - center) < radius:
            return True
        else:
            return False

    def walk_in_ball(self, center=None, radius=1, norm=lp_norm):
        """ movement of the brownie until it exits the ball with a given radius and center
        :param center: np.array with shape (n,), center of the ball
        :param radius: float, sphere radius
        :param norm: callable, any function that is a norm
        :return:
        """
        if not center:
            center = np.zeros(shape=(self.dim,))
        self.point = np.copy(center)
        while self.is_in_ball(self.point, center, radius, norm):
            self.random_walk_normal_step()

    def walk_in_area(self, start_point, is_in_area):
        """ brownie movement until exit from the given area
        :param start_point: numpy.array with shape (n,), starting point
        :param is_in_area: callable, function determining whether a point belongs to an area
        :return:
        """
        self.point = np.copy(start_point)
        while is_in_area(self.point):
            self.random_walk_normal_step()
