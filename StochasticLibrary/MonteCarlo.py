import numpy as np
from scipy.stats import norm


class MonteCarlo:
    """ a class for calculating Monte Carlo simulations
    """
    def __init__(self, function, dim=2, n_samples=100, gen_sample=None):
        """ constructor
        :param function: callable, function to integrate
        :param dim: int, dimensional of domain
        :param n_samples: int, number of single simulations
        :param gen_sample: callable, function generating samples from a given distribution
        """
        self.function = function
        self.dim = dim
        self.mean = None
        self.n_samples = n_samples
        self.variance = None
        self.gen_sample = gen_sample if gen_sample else np.random.random(dim)

    @staticmethod
    def quick_simulate(function, dim, n_samples=100, gen_sample=None):
        """ carries out simulations and returns the average
        :param function: callable, function to integrate
        :param dim: int, dimensional of domain
        :param n_samples: int, number of single simulations
        :param gen_sample: callable, function generating samples from a given distribution
        """
        if gen_sample is None:
            gen_sample = np.random.random
        results = []
        for _ in range(n_samples):
            results.append(function(gen_sample(dim)))
        return np.array(results).mean()

    def simulate(self):
        """ carries out simulations, returns the average and save variance for further analysis
        """
        results = []
        for _ in range(self.n_samples):
            results.append(self.function(self.gen_sample(self.dim)))
        results = np.array(results)
        self.mean = results.mean()
        self.variance = np.var(results)
        return self.mean

    def confidence_interval(self, alpha=0.05, var=None):
        """ calculates the asymptotic confidence interval with a given confidence level
        :param alpha: float from [0,1], confidence level
        :param var: float, theoretical value of the variance, by default the empirical value is calculated
        """
        var = var if var else self.variance
        ends = norm.ppf(1 - alpha) * np.sqrt(var) / np.sqrt(self.n_samples)
        return self.mean - ends, self.mean + ends
