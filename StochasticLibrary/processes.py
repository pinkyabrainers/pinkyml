import numpy as np


class Wiener:
    """ Wiener process generator
    """
    def __init__(self, dim=1):
        """
        :param dim: process dimension
        :return:
        """
        self.dim = dim
        self.trace = None

    @staticmethod
    def generate(start_point=None, time=1, division=500):
        """ internal function generating the process
        :param start_point: numpy.array with shape (n,), starting point of the process
        :param time: time interval, if time < 0, then function will be still generating process
        :param division: number of intermediate points in a unit time segment
        :return: generator generating successive realizations of the process
        """
        yield start_point
        if time < 0:
            while True:
                start_point = start_point + np.random.normal(loc=0.0, scale=1 / np.sqrt(division),
                                                             size=start_point.shape)
                yield start_point
        else:
            for _ in range(int(time * division)):
                start_point = start_point + np.random.normal(loc=0.0, scale=1 / np.sqrt(division),
                                                             size=start_point.shape)
                yield start_point


class Poisson:
    """ Poisson process generator
    """
    def __init__(self, dim=1):
        """
        :param dim: process dimension
        :return:
        """
        self.dim = dim
        self.trace = None

    @staticmethod
    def _generate(lam, start_point, time=1, division=500):
        """ internal function generating the process
        :param lam: float, the intensity of the process
        :param start_point: numpy.array with shape (n,), starting point of the process
        :param time: time interval, if time < 0, then function will be still generating process
        :param division: number of intermediate points in a unit time segment
        :return: generator generating successive realizations of the process
        """
        yield start_point
        for _ in range(int(time * division)):
            start_point = start_point + np.random.poisson(lam/division, size=start_point.shape)
            yield start_point

    def generate(self, lam=1, start_point=None, time=1, division=500):
        """ generates the process on the interval [0, time]
        :param lam: float, the intensity of the process
        :param start_point: numpy.array with shape (n,), starting point of the process
        :param time: time interval
        :param division: number of intermediate points in a unit time segment
        :return: generator generating successive realizations of the process
        """
        if not start_point:
            start_point = np.zeros(shape=(self.dim,))
        self.trace = self._generate(lam, start_point, time, division)
