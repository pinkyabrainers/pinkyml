from unittest import TestCase
from StochasticLibrary.brownian_motion import BrownianMotion


class TestBrownianMotion(TestCase):

    def test_random_walk_step(self):
        motion_5dim = BrownianMotion(dim=5, save_trace=True)
        motion_1dim = BrownianMotion(dim=1, save_trace=True)
        motion_5dim.random_walk_step()
        motion_1dim.random_walk_step()
        self.assertTrue(len(motion_5dim.trace) == 1)
        self.assertTrue(len(motion_1dim.trace) == 1)

    def test_random_walk_normal_step(self):
        motion_5dim = BrownianMotion(dim=5, save_trace=True)
        motion_1dim = BrownianMotion(dim=1, save_trace=True)
        motion_5dim.random_walk_normal_step()
        motion_1dim.random_walk_normal_step()
        self.assertTrue(len(motion_5dim.trace) == 1)
        self.assertTrue(len(motion_1dim.trace) == 1)

    def test_clean_trace(self):
        motion_5dim = BrownianMotion(dim=5, save_trace=True)
        motion_1dim = BrownianMotion(dim=1, save_trace=True)
        motion_1dim.clean_trace()
        motion_5dim.clean_trace()
        self.assertTrue(len(motion_5dim.trace) == 0)
        self.assertTrue(len(motion_1dim.trace) == 0)
