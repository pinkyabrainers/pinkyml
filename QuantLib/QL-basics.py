# import QuantLib.QuantLib as ql
from QuantLib import *


def print_date_access(date):
    """
    Prints basic date operation in QL, access to day, month,
    year and weekday
    :param date: QL date object
    """
    print(type(date))
    print(date.month(), date.dayOfMonth(), date.year())
    print(date.weekday())
    if date.weekday() == 5:
        print("It's 5 day in the week")


def print_arithmetic_op(date):
    """
    Prints basic date operations in QL, concatenating days
    and periods using Period() method
    :param date: QL date object
    """
    print("Add a day : ", date + 1)
    print("Subtract a day : ", date - 1)
    print("Add a week : ", date + Period(1, Weeks))
    print("Add a month : ", date + Period(1, Months))
    print("Add a year : ", date + Period(1, Years))


def print_logical_op(date):
    """
    Prints basic date arithmetic operations in QL:
    equal, greater, less, diff
    :param date: QL date object
    """
    print(date == Date(31, 3, 2015))
    print(date > Date(30, 3, 2015))
    print(date < Date(1, 4, 2015))
    print(date != Date(1, 4, 2015))


def calendar_add_period(date, period, calendar_1, calendar_2):
    """
    Adds period to given date in raw date,
    in first and second calendar nad prints it.
    :param date: QL date object
    :param period: QL period object
    :param calendar_1: QL calendar object
    :param calendar_2: QL calendar object
    """
    raw_date = date + period                                         # adds 60 days to date
    us_date = calendar_1.advance(date, period)                       # adds 60 business days in US
    poland_date = calendar_2.advance(date, period)                   # adds 60 business days in PL
    print(f'Raw: {raw_date}\nUS: {us_date}\nPL: {poland_date}')


def calendar_cal_bday(date, period, calendar_1, calendar_2):
    """
    Calculates business days between date and period and
    prints them.
    :param date: QL date object
    :param period: QL period object
    :param calendar_1: QL calendar object
    :param calendar_2: QL calendar object
    """
    cal_1_bdays = calendar_1.businessDaysBetween(date, date + period)
    cal_2_bdays = calendar_2.businessDaysBetween(date, date + period)
    print(f'Business days US:",{cal_1_bdays}, Business days PL: ",{cal_2_bdays}')
    # addHoliday() and removeHoliday() for fixing calendar errors


def joint_calendar(calendar_1, calendar_2, date, period):
    """
    Combines the holidays of two calendars
    Prints date + period in joined calendar
    and business days between date and joined date
    :param calendar_1: QL calendar object
    :param calendar_2: QL calendar object
    :param date: QL date object
    :param period: QL period object
    """
    joint_calendar = JointCalendar(calendar_1, calendar_2)
    joint_date = joint_calendar.advance(date, period)
    joint_bdays = joint_calendar.businessDaysBetween(date, joint_date)
    print("Add 60 business days in US-Poland:", joint_date)
    print("Business days US-Poland:", joint_bdays)


def create_schedule_1():
    """
    Generates a Schedule object containing dates between effective_date
    and termination_date with the tenor specifying the Period to be Monthly
    Schedule(const Date& effectiveDate,
             const Date& terminationDate,
             const Period& tenor,
             const Calendar& calendar,
             BusinessDayConvention convention,
             BusinessDayConvention terminationDateConvention,
             DateGeneration::Rule rule,
             bool endOfMonth,
             const Date& firstDate = Date(),
             const Date& nextToLastDate = Date())
    :return: QL Schedule object
    """
    effective_date = Date(1, 1, 2015)
    termination_date = Date(1, 1, 2016)
    tenor = Period(Monthly)
    calendar = UnitedStates()
    business_convention = Following
    termination_b_convention = Following
    date_generation = DateGeneration.Forward
    end_of_month = False

    schedule = Schedule(effective_date, termination_date, tenor, calendar,
                        business_convention, termination_b_convention,
                        date_generation, end_of_month
                        )
    return schedule


def create_schedule_2():
    """
    Generates a Schedule object containing dates between effective_date
    and termination_date with the tenor specifying the Period to be Monthly
    first_date adds short stub in the front of January, DateGeneration.Backward
    sets counting from effective date, not from the first day
    :return: QL Schedule object
    """
    effective_date = Date(1, 1, 2015)
    termination_date = Date(1, 1, 2016)
    first_date = Date(15, 1, 2015)
    tenor = Period(Monthly)
    calendar = UnitedStates()
    business_convention = Following
    termination_b_convention = Following
    date_generation = DateGeneration.Backward
    end_of_month = False

    schedule = Schedule(effective_date, termination_date, tenor, calendar,
                        business_convention, termination_b_convention,
                        date_generation, end_of_month, first_date
                        )
    return schedule


def create_schedule_3():
    """
    Generates a Schedule object containing dates between effective_date
    and termination_date with the tenor specifying the Period to be Monthly
    penultimate_date=nextToLastDate adds short stub before last date
    :return: QL Schedule object
    """
    effective_date = Date(1, 1, 2015)
    termination_date = Date(1, 1, 2016)
    penultimate_date = Date(15, 12, 2015)
    tenor = Period(Monthly)
    calendar = UnitedStates()
    business_convention = Following
    termination_b_convention = Following
    date_generation = DateGeneration.Forward
    end_of_month = False

    schedule = Schedule(effective_date, termination_date, tenor, calendar,
                        business_convention, termination_b_convention,
                        date_generation, end_of_month, Date(), penultimate_date
                        )
    return schedule


def create_schedule_4():
    """
    Generates a Schedule object from list of dates
    Schedule(const std::vector<Date>&,
             const Calendar& calendar,
             BusinessDayConvention rollingConvention)
    :return: QL Schedule object
    """
    dates = [Date(2, 1, 2015), Date(2, 2, 2015),
             Date(2, 3, 2015), Date(1, 4, 2015),
             Date(1, 5, 2015), Date(1, 6, 2015),
             Date(1, 7, 2015), Date(3, 8, 2015),
             Date(1, 9, 2015), Date(1, 10, 2015),
             Date(2, 11, 2015), Date(1, 12, 2015),
             Date(4, 1, 2016)]
    rolling_convention = Following
    calendar = UnitedStates()
    schedule = Schedule(dates, calendar,
                        rolling_convention)
    return schedule


def print_schedule(schedule):
    """
    Prints given schedule
    :param schedule: QL Schedule object
    """
    for i, d in enumerate(schedule):
        print(i + 1, d)


def calculate_interest_rate():
    """
    Class InterestRate() can be used to store the interest rate with the compounding type,
    day count and the frequency of compounding
    """
    annual_rate = 0.05
    day_count = ActualActual()
    compound_type = Compounded
    frequency = Annual

    interest_rate = InterestRate(annual_rate,
                                 day_count,
                                 compound_type,
                                 frequency)
    return interest_rate


def print_interest_rate(interest_rate):
    """
    compoundFactor() gives you how much your investment will be worth
    after any period.
    discountFactor() method returns the reciprocal of the compoundFactor.
    Useful while calculating the present value of future cashflows
    :param interest_rate: InterestRate() QL object
    """
    t = 2.0
    print(f'CompoundFactor() for t = 2: {interest_rate.compoundFactor(t)}')
    print(f'(1 + 0.05) * (1.0 + 0.05): {(1 + 0.05) * (1.0 + 0.05)}')
    print(f'discountFactor() for t = 2: {interest_rate.discountFactor(t)}')
    print(f'1.0/interest_rate.compoundFactor(t)): {1.0/interest_rate.compoundFactor(t)}')


def change_ir_frequency(interest_rate):
    """
    Converts frequency in given interest rate using the equivalentRate method
    :param interest_rate: QL InterestRate object
    """
    new_frequency = Semiannual
    compound_type = Compounded
    t = 2.0
    new_interest_rate = interest_rate.equivalentRate(compound_type, new_frequency, t)
    new_annual_rate = new_interest_rate.rate()

    return new_annual_rate


if __name__ == '__main__':
    """
    Introduction to the basics of QuantLib. Explained the Date, 
    Schedule, Calendar and InterestRate classes.
    """
    date = Date(6, 3, 2020)     # Date(day, month, year)
    today = Date().todaysDate()
    period = Period(60, Days)
    us_calendar = UnitedStates()
    poland_calendar = Poland()
    print(f'today is {today}')
    print_date_access(today)
    print_arithmetic_op(today)
    print_logical_op(today)
    calendar_add_period(today, period, us_calendar, poland_calendar)
    calendar_cal_bday(today, period, us_calendar, poland_calendar)
    joint_calendar(us_calendar, poland_calendar, today, period)
    schedule_example_1 = create_schedule_1()
    schedule_example_2 = create_schedule_2()
    schedule_example_3 = create_schedule_3()
    schedule_example_4 = create_schedule_4()
    print_schedule(schedule_example_1)
    print_schedule(schedule_example_2)
    print_schedule(schedule_example_3)
    print_schedule(schedule_example_4)
    print(calculate_interest_rate())
    interest_rate_example_1 = calculate_interest_rate()
    print_interest_rate(interest_rate_example_1)
    interest_rate_example_2 = change_ir_frequency(interest_rate_example_1)
    print(interest_rate_example_2)
