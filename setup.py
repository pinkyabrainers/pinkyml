from distutils.core import setup

setup(name='pinkybrainers',
      version='1.0',
      description='Python package with advance math implementations',
      author='Marcin Baranek, Kamil Bartocha',
      author_email='baranekmarcin47@gmail.com, kamilbartocha53@gmail.com',
      url='https://gitlab.com/pinkyabrainers',
      packages=['StochasticLibrary', 'Norm'],)
