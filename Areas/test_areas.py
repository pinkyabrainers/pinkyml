from unittest import TestCase
import numpy as np

from Areas.areas import Ball, Cube


class TestBall(TestCase):
    def test_area(self):
        ball1 = Ball()
        ball2 = Ball(center=np.array([1, 0, 6]), radius=3)
        self.assertEquals(ball1.area(), np.pi)
        self.assertEquals(ball2.area(), 36 * np.pi)

    def test_is_in_area(self):
        ball = Ball()
        points = [np.array([1, 0]), np.array([0.5, 0.7]), np.array([3, 5])]
        corrects = [False, True, False]
        for point, correct in zip(points, corrects):
            self.assertTrue(ball.is_in_area(point) == correct)

    def test_grid(self):
        self.fail()

    def test_distance(self):
        ball = Ball()
        points = [np.array([1, 0]), np.array([0.5, 0.7]), np.array([3, 4])]
        corrects = [0.0, 1 - np.sqrt(0.74), 4]
        for point, correct in zip(points, corrects):
            self.assertEqual(ball.distance(point), correct)


class TestCube(TestCase):
    def test_area(self):
        cube1 = Cube()
        cube2 = Cube(center=np.array([1, 0, 6]), side=3)
        self.assertEquals(cube1.area(), 4)
        self.assertEquals(cube2.area(), 27)

    def test_is_in_area(self):
        cube = Cube()
        points = [np.array([1, 0]), np.array([0.5, 0.7]), np.array([3, 5])]
        corrects = [False, True, False]
        for point, correct in zip(points, corrects):
            self.assertTrue(cube.is_in_area(point) == correct)

    def test_grid(self):
        self.fail()

    def test_distance(self):
        cube = Cube()
        points = [np.array([1, 0]), np.array([0.5, 0.7]), np.array([3, 4])]
        corrects = [0.0, 0.3, np.sqrt(13)]
        for point, correct in zip(points, corrects):
            self.assertAlmostEqual(cube.distance(point), correct)
