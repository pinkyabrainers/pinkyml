import numpy as np
import math

from Norm.norms import lp_norm
from Functions.functions import double_factorial


class Area:
    """ polymorphic class describing areas in a dim dimensional space
    """
    def __init__(self, dim=2):
        """ constructor
        :param dim: space dimension
        :return:
        """
        self.dim = dim

    def area(self):
        """ calculates the volume of the area
        """
        pass

    def is_in_area(self, point, strong=True):
        """ returns True if the point belongs to an area
        :param point: numpy.array with shape (self.dim,)
        :param strong: bool, if False we mean area as area with a border
        :return: bool, True or False
        """
        pass

    @staticmethod
    def is_in_unit(point, strong=True):
        """ returns True if the point belongs to an area
        :param point: numpy.array with shape (dim,)
        :param strong: bool, if False we mean area as area with a border
        :return: bool, True or False
        """
        pass

    def mesh(self):
        """ returns the area mesh generator
        """
        pass

    def distance(self, point):
        """ returns the distance of a point from the edge in norm l2
        :param point: numpy.array with shape (self.dim,)
        :return: float, distance
        """
        pass

    @staticmethod
    def gen_random(dim=2):
        """ generates a point from a uniform distribution over an unit area
        """
        pass

    @staticmethod
    def gen_sphere_random(dim=2):
        """ generates a point from a uniform distribution over an unit sphere
        """
        pass


class Ball(Area):
    def __init__(self, dim=2, center=None, radius=1):
        super().__init__(dim) if center is None else super().__init__(center.shape[0])
        self.center = center if center is not None else np.zeros(shape=(self.dim,))
        self.radius = radius

    def area(self):
        if self.dim % 2 == 0:
            return np.power(np.pi, self.dim / 2) * np.power(self.radius, self.dim) / math.factorial(self.dim / 2)
        else:
            k = (self.dim + 1) / 2
            return 2 ** k * np.power(np.pi, k - 1) * (self.radius**self.dim) / double_factorial(self.dim)

    def is_in_area(self, point, strong=True):
        return True if lp_norm(point - self.center) < self.radius else False if strong else\
            True if lp_norm(point - self.center) <= self.radius else False

    @staticmethod
    def is_in_unit(point, strong=True):
        """ returns True if the point belongs to an unit Ball
        :param point: numpy.array with shape (dim,)
        :param strong: bool, if False we mean area as area with a border
        :return: bool, True or False
        """
        return True if lp_norm(point) < 1 else False if strong else \
            True if lp_norm(point) <= 1 else False

    def mesh(self, num=50):
        # TODO
        raise NotImplemented

    def distance(self, point):
        return np.abs(lp_norm(point - self.center) - self.radius)

    @staticmethod
    def gen_random(dim=2):
        sample = 2 * np.random.random(dim) - np.ones(shape=(dim,))
        while lp_norm(sample) > 1:
            sample = 2 * np.random.random(dim) - np.ones(shape=(dim,))
        return sample

    @staticmethod
    def gen_sphere_random(dim=2):
        """ generates a point from a uniform distribution over an unit sphere
        """
        point = Ball.gen_random(dim)
        return point / lp_norm(point)


class Cube(Area):
    def __init__(self, dim=2, center=None, side=2):
        super().__init__(dim) if center is None else super().__init__(center.shape[0])
        self.center = center if center is not None else np.zeros(shape=(self.dim,))
        self.side = side

    def area(self):
        return np.power(self.side, self.dim)

    def is_in_area(self, point, strong=True):
        return True if lp_norm(point - self.center, p=-1) < self.side / 2 else False if strong else\
            True if lp_norm(point - self.center, p=-1) <= self.side / 2 else False

    @staticmethod
    def is_in_unit(point, strong=True):
        """ returns True if the point belongs to unit Cube, we mean unit cube as [-1, 1]^dim
        :param point: numpy.array with shape (dim,)
        :param strong: bool, if False we mean area as area with a border
        :return: bool, True or False
        """
        return True if lp_norm(point, p=-1) < 1 else False if strong else \
            True if lp_norm(point, p=-1) <= 1 else False

    def mesh(self):
        # TODO
        raise NotImplemented

    def distance(self, point):
        if self.is_in_area(point, strong=False):
            return (self.side / 2) - lp_norm(point - self.center, p=-1)
        else:
            helping_point = (point - self.center)[np.where(np.abs(point - self.center) > self.side / 2)]
            return lp_norm(np.abs(helping_point) - (self.side / 2) * np.ones(shape=helping_point.shape))

    @staticmethod
    def gen_random(dim=2):
        return 2 * np.random.random(dim) - np.ones(shape=(dim,))

    @staticmethod
    def gen_sphere_random(dim=2):
        """ generates a point from a uniform distribution over an unit sphere
        """
        # TODO
        raise NotImplemented
