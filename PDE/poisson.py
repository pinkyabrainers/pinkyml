import numpy as np

from Norm.norms import lp_norm
from StochasticLibrary.processes import Wiener
from Areas.areas import Ball


class StochasticMethod:
    """ Abstract class for solving poisson differential equations by stochastic methods
    -Δu = f in Ω
    u = g on ∂Ω
    """

    def __init__(self, f_function, g_function, domain):
        """ constructor
        :param f_function: callable, describing function -Δu
        :param g_function: callable, describing function u on ∂Ω
        :param domain: an object of type Area, should have the method:
        distance, calculating the distance of a point from the edge
        """
        self.f = f_function
        self.g = g_function
        self.domain = domain

    def simulate(self, *args):
        """ performs simulations
        """
        pass

    def confidence_interval(self, *args):
        """ computes confidence intervals for the results
        """
        pass


class WalkOnSphereMethod(StochasticMethod):
    """ class for solving poisson differential equations by walk on sphere method

    -Δu = f in Ω
    u = g on ∂Ω

    the method is based on equality
    =================================================================================
    u(x) = E g(X_τΩ) + 0.5 E ∫ f(X_t)dt from 0 to τΩ, where

    τΩ is a exit time from Ω of process X_t = x + W_t, and W_t is a wiener process
    =================================================================================
    """

    def __init__(self, f_function, g_function, domain):
        """ constructor
        :param f_function: callable, describing function -Δu
        :param g_function: callable, describing function u on ∂Ω
        :param domain: an object of type Area, should have the method:
        distance, calculating the distance of a point from the edge
        """
        super().__init__(f_function, g_function, domain)

    @staticmethod
    def int_through_unit_ball(function, dim, division=10):
        """ compute ∫ f(W_t) dt from 0 to τ where τ is the time of the exit
         of the wiener process W from the unit dim-dimensional ball
        :param function: callable, function f to integrate
        :param dim: int, dimensional
        :param division: int, number of intermediate points in a unit time
         segment of the  process W, for more see StochasticLibrary/processes/Wiener
        :return float,
        """
        walk = Wiener.generate(start_point=np.zeros(shape=(dim,)), time=-1, division=division)
        flag, results = True, []
        while flag:
            point = walk.__next__()
            results.append(function(point))
            if Ball.is_in_unit(point, strong=True):
                flag = False
        return np.array(results).mean() / dim

    @staticmethod
    def generate_walk(start_point, area, epsilon=1.e-5):
        """ generates a discrete walk around the area starting from the starting point
         where each step is recursively computed:
         next = last + dist (last) * sample, where
         dist (last) the distance of the last point from the bound of the area
         sample is a random point from a sphere with a dimension equal to the dimension start_point
         :param start_point: np.array with shape (dim,), is a start point
         :param area: an object of type Area, should have the method:
          distance, calculating the distance of a point from the edge
         :param epsilon: float, the function stops generating points when
          the distance of the point from the bound of the area is less than or equal to the epsilon
         """
        yield start_point
        while area.distance(start_point) > epsilon:
            step = Ball.gen_random(start_point.shape[0])
            start_point = start_point + area.distance(start_point) * step / lp_norm(step)
            yield start_point

    def __sum_along_the_walk(self, walk, division=10, return_last=False):
        """ sums over k: ∫ f(x_k + r * W_t) dt from 0 to τ where τ is the time of the exit
         of the wiener process W from the unit dim-dimensional ball, where x_k is a step of walk
         :param walk: generator generating next steps or array with steps
         :param division:int, number of intermediate points in a unit time
          segment of the  process W, for more see StochasticLibrary/processes/Wiener
         :param return_last: bool, if True returns sum and last point from walk else
          return only sum
         :return: (float, np.array) or float se param return_last
         """
        results, radius, last = [], 0, None
        for step in walk:
            radius = self.domain.distance(step)
            results.append(radius * radius * WalkOnSphereMethod.int_through_unit_ball(
                lambda arg: self.f(step + radius * arg), dim=step.shape[0], division=division))
            last = step
        return (np.array(results).sum(), last) if return_last else np.array(results).sum()

    def simulate(self, point, mc_iter=100, division=100, epsilon=1.e-8):
        """ performs simulations of the method based on a walk through the sphere
        :param point: np.array, start point for simulation
        :param mc_iter: int, number of simulations
        :param division: int, number of intermediate points in a unit time
         segment of the  process W, for more see StochasticLibrary/processes/Wiener
        :param epsilon: float, method stops generating points when
         the distance of the point from the bound of the area is less than or equal to the epsilon
        """
        total, g_results = [], []
        for _ in range(mc_iter):
            walk = WalkOnSphereMethod.generate_walk(point, self.domain, epsilon=epsilon)
            integrate_value, last = self.__sum_along_the_walk(walk, division=division, return_last=True)
            g_results.append(self.g(last))
            total.append(np.array(integrate_value).sum())
        return np.array(g_results).mean() + 0.5 * np.array(total).mean()
